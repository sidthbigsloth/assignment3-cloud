package main //currency converter

import (
	"bytes"
	"encoding/json"
	"net/http"
	"os"
	"strconv"

	"google.golang.org/appengine"
	"google.golang.org/appengine/urlfetch"
)

type dialogflowJSON struct {
	Result struct {
		Param struct {
			Amount float64 `json:"amount"`
			From   string  `json:"currency-from"`
			To     string  `json:"currency-to"`
		} `json:"paramaters"`
	} `json:"result"`
}

type currencies struct {
	Base   string `json:"baseCurrency"`
	Target string `json:"targetCurrency"`
}

type response struct {
	Speech      string `json:"speech"`
	Displaytext string `json:"displayText"`
}

func converter(w http.ResponseWriter, r *http.Request) {
	//http.Header.Add(w.Header(), "content-type", "application/json"

	var d dialogflowJSON
	var c currencies
	var resp response
	//because assignment 2 didn't work for us, we use Nataniel Gåsøy's assignment 2 with his permission
	url := os.Getenv("URL")
	err := json.NewDecoder(r.Body).Decode(&d)

	if err != nil {
		panic(err)
	}
	if d.Result.Param.Amount == 0 {
		d.Result.Param.Amount = 1
	}

	c.Base = d.Result.Param.From
	c.Target = d.Result.Param.To

	ctx := appengine.NewContext(r)
	client := urlfetch.Client(ctx)

	toSend, _ := json.Marshal(c)

	page, _ := client.Post(url, "application/json", bytes.NewReader(toSend))

	var rate float64
	_ = json.NewDecoder(page.Body).Decode(&rate)

	result := d.Result.Param.Amount * rate

	str := c.Base
	str += "to"
	str += c.Target
	str += "is"
	str += strconv.FormatFloat(float64(result), 'f', -1, 32)
	str += "."
	if result == 0 {
		str = " currency not supported"
	}

	resp.Displaytext = str
	resp.Speech = str

	json.NewEncoder(w).Encode(resp)
}

func main() {
	http.HandleFunc("/", converter)
	http.ListenAndServe(os.Getenv("PORT"), nil)

	appengine.Main()
}
